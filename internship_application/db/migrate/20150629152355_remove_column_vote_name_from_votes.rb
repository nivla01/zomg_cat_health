class RemoveColumnVoteNameFromVotes < ActiveRecord::Migration
  def change
    remove_column :votes, :vote_name, :string
  end
end
