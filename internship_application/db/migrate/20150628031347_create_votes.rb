class CreateVotes < ActiveRecord::Migration
  def change
    create_table :votes do |t|
      t.integer :cat_id
      t.string :ip
      t.string :vote_name

      t.timestamps
    end
  end
end
