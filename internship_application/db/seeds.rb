# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
User.destroy_all
User.create(email:"demo@demo.com",password:"demo@demo.com",password_confirmation:"demo@demo.com")

Vote.destroy_all

Cat.destroy_all

Cat.create( url:"http://dg4l0lhura2yj.cloudfront.net/static/images/sitewide/graphics/good-cat.jpg")
Cat.create( url:"http://files.dogster.com/pix/cats/12/1276212/lg_1276212_1353705080.jpg")
Cat.create( url:"http://files.dogster.com/pix/cats/37/1308637/lg_1308637_1380377074.jpg")
Cat.create( url:"http://files.dogster.com/pix/cats/61/1122961/lg_1122961_1319325884.jpg")
Cat.create( url:"http://files.dogster.com/pix/cats/39/1308639/lg_1308639_1380378569.jpg")
Cat.create( url:"http://files.dogster.com/pix/cats/01/1282901/lg_1282901_1358736292.jpg")
Cat.create( url:"http://files.dogster.com/pix/cats/33/1329533/lg_1329533_1417730934.jpg")

VoteType.destroy_all

VoteType.create( name:"ZOMG! So Cute")
VoteType.create( name:"Amazing! So Beautiful!")