class CatsController < ApplicationController
  def index
    @cats = Cat.all
  end
  def new
    @cat = Cat.new
  end
  def show
    @cat = Cat.find params[:id]
  end
  def edit
    @cat = Cat.find params[:id]
  end
  
  def create
    @cat  = Cat.new cat_params
    if @cat.save
      flash[:success] = "（ΦωΦ）Cat created corretcly meow （ΦωΦ）!!!"
      redirect_to cats_path
    else
      render 'new'
    end
  end

  def update
    @cat = Cat.find params[:id]
    if @cat.update cat_params
      flash[:success] = "（ΦωΦ） update successfully （ΦωΦ）"
      redirect_to @cat
    else
      render 'edit'
    end
  end
  
  def destroy
    Cat.destroy(params[:id])
    flash[:info] = "(ↀДↀ)）Cat deleted (ↀДↀ)"    
    redirect_to cats_path
  end

  private
  def cat_params
    params.require(:cat).permit(:url)
  end
end
