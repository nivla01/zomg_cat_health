class VotesController < ApplicationController
  before_action :verify_vote_types_count
  
  def index
    @votes = Vote.all_votes
    respond_to do |format|
      format.csv { send_data @votes.to_csv }
    end
  
  end
  
  def new
    @votes_types =  VoteType.all
    @cats = Cat.offset(next_page).limit(2)
  end
  
  def create
    @vote = Vote.new(cat_id: params[:cat_id],vote_type_id: params[:vote_type_id], ip: request.remote_ip)
    if @vote.save
      flash[:success] = 'Thanks for vote!!!!!'
      redirect_to root_path
    else
      render 'new'
    end
  end
  
  private
  def next_page()
    if cookies[:next_cats]
     cookies[:next_cats]  =cookies[:next_cats].to_i + 2
      cookies[:next_cats] = (cookies[:next_cats].to_i > Cat.all.count) ? 0 : cookies[:next_cats]
    else
      cookies[:next_cats] = 0
    end
  end
  def verify_vote_types_count
    render text:"please run rake db:seed before run the application" if VoteType.count < 2
  end
end
