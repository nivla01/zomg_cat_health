class ReportsController < ApplicationController
  def index
    vote_type = (params[:order_by].present?) ? VoteType.find_by_name(params[:order_by]) : VoteType.first
  
    @vote_types = VoteType.all
    @top_cats = Vote.ranked_by vote_type.id
  end
end
