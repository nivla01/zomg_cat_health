class Vote < ActiveRecord::Base
    validates_presence_of :cat_id,:ip,:vote_type_id
    #validates :vote_name,inclusion: { in: ["Amazing! So Beautiful!","ZOMG! So Cute!"] }
    belongs_to :cat
    belongs_to :vote_type
    
    def self.to_csv 
        CSV.generate do |csv|
           csv << [:url,:ip,:vote]
            all_votes.each do |vote|
                csv << [vote.url,vote.ip,vote.name]
            end
        end
    end
    
    def self.all_votes
       joins(:cat,:vote_type)
       .select(:url,:ip,:name)
        
    end
    
    def self.ranked_by(vote_type_id)
        joins(:cat,:vote_type)
        .group("Cats.url","vote_types.name")
        .where("vote_type_id = ?",vote_type_id)
        .order("count_all desc")
        .count
    end
end
