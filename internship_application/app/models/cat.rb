class Cat < ActiveRecord::Base
    validates_presence_of :url
    has_many :votes
end
