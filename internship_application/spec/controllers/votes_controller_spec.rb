require 'rails_helper'

RSpec.describe VotesController, type: :controller do
  before(:each) do
    cat = FactoryGirl.create(:cat)
    FactoryGirl.create(:vote_type,name:"AMAZING")
    vote_type = FactoryGirl.create(:vote_type)
    FactoryGirl.create(:vote,cat: cat,vote_type: vote_type)
  end
  pending "test csv"
  
  describe "GET #index" do

    it "returns http success" do
      get :index,format: :csv
      expect(response).to have_http_status(:ok)
    end
    
  end
  describe "GET #new" do
    it "returns http success" do
      get :new
      expect(response).to have_http_status(:ok)
    end
  end
  
  describe 'POST #create' do
    it 'create a vote for cat' do
      expect{
        post :create,{ cat_id:5,vote_type_id:1}
      }.to change{Vote.count}.by 1
      expect(response).to redirect_to(root_path)
    end

  end

end
