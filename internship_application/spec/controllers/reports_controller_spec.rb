require 'rails_helper'

RSpec.describe ReportsController, type: :controller do
  pending "test for displaying ranking cats #{__FILE__}"
  describe "GET #index" do
    before(:each) do
      cute = FactoryGirl.create(:vote_type, name:"ZOMG! So Cute")
      amazing = FactoryGirl.create(:vote_type, name:"Amazing! So Beautiful!")
      
      cat = FactoryGirl.create(:cat)
      cat_2 = FactoryGirl.create(:cat)
      
      FactoryGirl.create(:vote, cat: cat,vote_type: cute,ip:"127.0.0.1")
      FactoryGirl.create(:vote, cat: cat,vote_type: cute,ip:"192.168.100.1")
      FactoryGirl.create(:vote, cat: cat,vote_type: amazing,ip:"172.16.70.131")
    end

    it "returns http success" do
      get :index
      expect(response).to have_http_status(:success)
    end
    
    # it "display cat ranking" do
    #   get :index
    # # expect(assigns(:reports)).to eq [{url: cat.url}]
    # end
  end

end
