require 'rails_helper'

RSpec.describe CatsController, type: :controller do
  
  describe "GET #index" do
    before(:each) do
        5.times { FactoryGirl.create(:cat)}
    end
    it "returns 5 cats" do
      get :index
      expect(assigns(:cats).count).to eq 5
      expect(response).to have_http_status(:success)
    end
  end
  describe "GET #new" do
    it 'assigns a new cat' do
      get :new
      expect(assigns(:cat)).to_not be nil
      expect(response).to render_template("new")
    end
  end
  describe "GET #edit" do
    it 'render form edit corretcly' do
      cat =FactoryGirl.create(:cat)
      get :edit, id:cat.id
      expect(response).to render_template("edit")
    end
  end
  
  describe "POST #create" do
    it 'create a cat' do
      expect{
        post :create,cat:{
          url:"http://feelgrafix.com/data_images/out/4/765011-cat-wallpapers.jpg"
          
        }
      }.to change{Cat.count}.by 1
      expect(response).to redirect_to(cats_path)
  
    end
    
    it 'render new template when invalid params' do
      post :create,cat:{ url:""}
      expect(assigns(:cat)).to be_a_new(Cat)
      expect(response).to render_template("new")
    end
  end
  
  describe 'PUT #edit/:id' do

    context "with valid parameters" do
      let(:attr) do
        { url:"https://jeffreyallenbooks.files.wordpress.com/2012/05/crazy-cat-fashions-sheriff-style-large-msg-1289331919051.jpg?w=590"}
      end
      it 'should edit corretcly' do
        cat = FactoryGirl.create(:cat)
        put :update,id: cat.id,cat: attr 
        cat.reload
        expect(cat.url).to eq attr[:url]
      end
    end
 
  end
  
  describe "DELETE #destroy/:id" do
    before(:each) { @cat = FactoryGirl.create(:cat)}
    it 'delete a cat' do
      expect{  delete :destroy,id: @cat }.to change{Cat.count}.by(-1)
    end
    
    it 'redirect to cats#index' do
      delete :destroy,id: @cat 
      expect(response).to redirect_to(cats_path)
    end

  
  end
  
  

end
