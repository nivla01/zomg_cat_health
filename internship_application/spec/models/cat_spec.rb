require 'rails_helper'

RSpec.describe Cat, type: :model do
    context 'creating a cat' do
        it 'should be succsess' do
            FactoryGirl.create(:cat)
        end
        
        it 'should be fail with invalid parameters' do
            cat = FactoryGirl.build(:cat,url:"")
            expect(cat.valid?).to be false
        end
    end
end
