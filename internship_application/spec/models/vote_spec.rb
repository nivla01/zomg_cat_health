require 'rails_helper'

RSpec.describe Vote, type: :model do
    context 'creating a vote' do
        it 'should be succsess' do
            FactoryGirl.create(:vote)
        end

    end
    
    context 'invalid vote' do
        %w(cat_id ip vote_type_id).each do |attr|
            it "should be fail without param #{attr}" do
                vote = Vote.new
                expect(vote.valid?).to be false
                expect(vote.errors[attr].present?).to be true
            end
        end
    end
end
